SELECT * FROM SALES_DATA  ;
--Butun zamanlarda nece sayda olkeye mehsul satilmisdir.
SELECT DISTINCT CUSTOMER_COUNTRY 
FROM SALES_DATA
ORDER BY CUSTOMER_COUNTRY ;
--Evvelki ilde daimi musterimiz olmus bu il satis etmediyimiz musterilerin sebeblerini nezerden kecirmek  
SELECT  CUSTOMER_NAME
FROM SALES_DATA
WHERE YEAR_ =2022
MINUS
SELECT CUSTOMER_NAME
FROM SALES_DATA
WHERE YEAR_ =2023
--Ortalama cekisi 50-den cox olan et mehsullarinin cesidleri 
SELECT PRODUCT_NAME,ROUND(AVG(PRODUCT_WEIGHT),0) 
FROM SALES_DATA
GROUP BY PRODUCT_NAME
HAVING ROUND(AVG(PRODUCT_WEIGHT),0) >50and PRODUCT_NAME LIKE 'Beef%'
order by PRODUCT_NAME;
--Kampaniyalarda satilan mehsullar
select SALE_ID,SOURCE_CAMPAIGN,substr(SOURCE_CAMPAIGN,1,1)||'.'||substr(SOURCE_CAMPAIGN,INSTR(SOURCE_CAMPAIGN,' ')+1,1)||'.'
from sales_data 
order by SOURCE_CAMPAIGN;
--2023 cu ilde 'MUSTERI ZIP'-i null olan deyerleri 'BOSDUR'ifadesi ile evez etmek
SELECT NVL(CUSTOMER_ZIP,'BOSDUR')
FROM SALES_DATA
WHERE YEAR_=2023;
--Catdirilma sirketlerini kodlamaq
select shipping_company,decode(shipping_company,'DHL',1,decode(shipping_company,'FedEx',2,'UPS',3,4)) 
from sales_data;
--Bos buraxilmis deyerleri  olan musteriler haqqinda melumatlar
select customer_email,customer_phone,customer_address,coalesce(customer_state,customer_zip,'!!!')
from sales_data
where year_=2023 and quarter in('Q2','Q3')
ORDER BY customer_address;
--Musterinin aldigi mehsul ve diger melumatlari data bazaya daxil edilmesi
insert into sales_data(month_,quarter,year_,customer_name,customer_city,product_name)
values('10','Q4','2023','Omarli Nurana','Baku','Chocolate');
commit;
--Yeni getirilmis mehsullarin kateqoriya adi deyisdirilerek bir kateqoriyada cemlenmesi
update sales_data
set product_category='Health and Cleaning' 
where product_category='Health';
commit;
--Musteri melumatlarinin yanlis daxil edilmesi sebebile melumatlarin silinmesi
delete  from sales_data 
where customer_name='Omarli Nurana';
commit;
-- Qeydiyyat tarixi ucun verilen 10 gun erzinde qeydiyyati nezere alinan musterilerin qeydiyyat nomreleri
select customer_id, subscription_end_date,subscription_start_date,extract(day from to_date(subscription_end_date,'mm/dd/yyyy'))-extract(day from to_date(subscription_start_date,'mm/dd/yyyy'))
from sales_data
where extract(day from to_date(subscription_end_date,'mm/dd/yyyy'))-extract(day from to_date(subscription_start_date,'mm/dd/yyyy'))<=10 and extract(day from to_date(subscription_end_date,'mm/dd/yyyy'))-extract(day from to_date(subscription_start_date,'mm/dd/yyyy'))>0