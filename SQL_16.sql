--Top 3 o emektaslari ekrana cixardin. Hansiki ,onlarin soyadinda olan simvollar daha cox olsun.

--
select * from employees 
order by length(last_name)desc
fetch next 3 rows only;
--Yoxlanmasi
select last_name,length(last_name) from employees
order by length(last_name)desc ;
--
select * from employees fetch first 3 rows only;
select * from employees offset 3 rows fetch next 3 rows only;
--
select * from employees;
select * from departments;
--Sirket uzre ortalamsai 50den cox olan departamentleri siyahisi

--
select e1.*,"Sirket uzre faiz nisbeti"||' '||'%'
from (select e.department_id,d.department_name,e.salary  "Emek haqqi",
round(avg(e.salary)over())   "Sirket uzre ortalama",round(e.salary*100/round(avg(e.salary)over()))  "Sirket uzre faiz nisbeti"
from employees e left join departments d
on e.department_id=d.department_id) e1 where "Sirket uzre faiz nisbeti">50 
order by "Sirket uzre faiz nisbeti" desc;

--Emektaslarin butun melumatlarini eks etdirmek.
select * from employees;
select * from departments;
select * from jobs;
select * from job_history;
select * from locations;
select * from countries;
select * from regions;

select * 
from employees e left join departments d on e.department_id=d.department_id
left join jobs j on e.job_id=j.job_id
left join locations l on d.location_id=l.location_id
left join countries c on l.country_id=c.country_id
left join regions r on c.region_id=r.region_id;
