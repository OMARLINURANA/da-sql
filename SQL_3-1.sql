select *from sales_data;
--lower
select lower(quarter) from sales_data;
--upper
select upper(customer_email)from sales_data;
--initcap
select initcap(product_description)from sales_data;
-- length
select length(customer_name)from sales_data;
--substr
select substr(customer_phone,9,4) from sales_data;
--instr
select instr(product_name,'B',2)from sales_data;
--LPAD
select LPAD(PRODUCT_ID,3,'0')from sales_data;
--RPAD
select RPAD(PRODUCT_ID,3,'0')from sales_data;
--TRIM
select TRIM(DISCOUNT_CODE)from sales_data;
--LTRIM
select LTRIM('      CC     '),'      CC     'from DUAL;
--LTRIM
select RTRIM('      CC     '),'      CC     'from DUAL;
--REPLACE
select REPLACE(DATE_,'/','-')from sales_data;
--
select REPLACE(CUSTOMER_NAME,' ','')from sales_data;
--
SELECT COUNT(*),QUARTER FROM SALES_DATA WHERE YEAR_=2023 GROUP BY QUARTER;
--
SELECT COUNT(*) FROM SALES_DATA;