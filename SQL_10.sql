1)Theoretical: Explain the concept of "SQL Joins".
Melumat artiqligi , anomaliyalarin yaranmamasi ve deyisikliklerin edilmesinde yaranacaq problemler sebebile
ayrilmis cedvellerde olan melumatlarin gerekli oldugunda birlikte gosterilmesi,mueyyen bir netice cixarilmasi ucun istifade edilir. 

2)Incomplete - Practical: Write a SQL query to fetch the details of employees who are working under manager '100' and earn more than 10000.

select * from employees;

SELECT employee_id,first_name,last_name,salary,manager_id
FROM employees
WHERE manager_id = 100 AND salary > 10000;

1)Problem-Solving: How would you find the job title that has the highest average salary?

select * from jobs;

select max(max_salary)
from jobs;
select job_title,max_salary 
from jobs 
where max_salary=40000;

select round(avg(max_salary),1) 
from jobs;

2)Complete - Practical: Write a SQL query to fetch the employees whose last name starts with the letter 'S'.
Hint: Use the LIKE operator.

select * 
from employees
where last_name like 'S%';

3)Incomplete - Practical: Write a SQL query to fetch the departments that have more than five employees.

select * from employees;
select * from DEPARTMENTS;
SELECT d.department_name, COUNT(e.employee_id) as employee_count
FROM departments d
right JOIN employees e
ON d.department_id = e.department_id
GROUP BY d.department_name
HAVING COUNT(e.employee_id) > 5;