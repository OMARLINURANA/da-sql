--------------------------------------------------------------------------------------------------------------------------------
ROLLUP / CUBE / GROUPING / GROUPING_ID                                                                                         -
--------------------------------------------------------------------------------------------------------------------------------

--2023 cu ilde 1-ci ve 2-ci rubunde her brendin satdigi umumi miqdarlari,umumi satis miqdarini 
--3-cu ve 4-cu rublerinde her brendin satdigi umumi miqdarlari,umumi satis miqdarlar? ile birlikte muqaiseli sekilde gostermek .

select  quarter,product_brand,sum(quantity) 
from sales_data 
where year_=2023 and   quarter  in ('Q1','Q2')
group by rollup(quarter,product_brand)

UNION ALL

select  quarter,product_brand,sum(quantity) 
from sales_data 
where year_=2023 and   quarter  in ('Q3','Q4')
group by rollup(quarter,product_brand);

--2023 cu ilde 1-ci ve 2-ci rubunde her brendin satis deyerini,umumi satis deyerini ve her brendin iki rubde umumilikte satis deyerini
--3-cu ve 4-cu rublerinde her brendin satis deyerini,umumi satis deyerini ve her brendin iki rubde umumilikte satis deyeri ile birlikte muqaiseli sekilde gostermek .

select  quarter,product_brand,sum(TOTAL_PRICE)
from sales_data
where year_=2023 and   quarter  in ('Q1','Q2')
group by CUBE(quarter,product_brand)

UNION ALL

select  quarter,product_brand,sum(TOTAL_PRICE)
from sales_data 
where year_=2023 and   quarter  in ('Q3','Q4')
group by cube(quarter,product_brand);

--

SELECT COUNT(*),CANCELLATION_REASON,CANCELLATION_CATEGORY 
FROM SALES_DATA
GROUP BY CUBE(CANCELLATION_REASON,CANCELLATION_CATEGORY);

--Son 4 ilde her rubluye uygun olaraq her kateqoriyanin satisi ,umumilikte her ilin ve her rubluyun butun kategoriyalar uzre total cemini,
--umumilikte 4 ildeki umumi cemi ve  butun kateqoriyalarin tek tek 4 ildeki cemini gostermek.

select sum(TOTAL_PRICE),year_,quarter,product_category, GROUPING_ID(year_,quarter,product_category)
from sales_data
where year_ IN(2020,2021,2022,2023) 
group by cube(year_,quarter,product_category);

--Her brendin  her kategoriyada mehsulun maksimal satis deyerini , brendlerde umumilikte mehsulun maksimal satis deyeri , 
--kateqoriyalarda umumilikte mehsulun maksimal satis deyeri ve butovlukte en yuksek satis deyerini  mueyyenlesdirmek

select max(unit_price),product_brand,product_category,grouping(product_brand),grouping(product_category)
from sales_data
group by cube(product_brand,product_category)
order by product_brand,product_category;

