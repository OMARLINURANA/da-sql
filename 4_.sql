select * 
from sales_data;
--
select customer_name,customer_email||'/'||customer_phone,billing_zip
from sales_data 
where billing_zip is null
ORDER BY CUSTOMER_NAME;
--
select product_category,upper(product_name),initcap(product_description),unit_price,product_weight 
from sales_data 
where product_category='Books' and unit_price<120
order by to_number(unit_price)DESC;
--2023 CU ILIN ILK YARISINDA   DAIR MELUMATLAR
SELECT DATE_ AS TARIX,LOWER(TIME_) AS ZAMAN,QUARTER AS RUBLUK,YEAR_ AS IL ,CUSTOMER_NAME AS "MUSTERI ADI" 
FROM SALES_DATA
WHERE QUARTER NOT IN('Q3','Q4')AND YEAR_=2023
ORDER BY DATE_,TIME_; 
-- SONU COM ILE BITEN MAIL UNVANLARI
SELECT CUSTOMER_NAME,INITCAP(CUSTOMER_EMAIL),SUBSTR(CUSTOMER_EMAIL,-3,3)
FROM SALES_DATA 
WHERE SUBSTR(CUSTOMER_EMAIL,-3,3)='com'
order by CUSTOMER_NAME;
--
SELECT LPAD(PRODUCT_ID,4,0),PRODUCT_CATEGORY,PRODUCT_SUBCATEGORY,PRODUCT_NAME 
FROM SALES_DATA;
--
SELECT CUSTOMER_NAME,LENGTH(CUSTOMER_NAME),CUSTOMER_EMAIL,LENGTH(CUSTOMER_EMAIL) 
FROM sales_data 
WHERE LENGTH(CUSTOMER_EMAIL)>=25 AND LENGTH(CUSTOMER_NAME)>10;
--
SELECT DELIVERY_DATE,DELIVERY_TIME,REPLACE(DELIVERY_STATUS,'Failed Delivery Attempt','!!!!!') 
FROM SALES_DATA ;
--
select gender as Cinsiyyet,count(gender) as "Organic Search-e uygun sayi"
from sales_data
where source_channel='Organic Search'
group by gender
order by gender;
--
select product_category,product_code,round(avg(product_weight),2),max(product_height)
from sales_data
group by product_category,product_code 
order by product_category
--
select CUSTOMER_ID,trim(SUBSTR(marital_status,1,3)) 
FROM SALES_DATA
ORDER BY TO_NUMBER(CUSTOMER_ID); 
--
select  CUSTOMER_ID,SUBSTR(TIME_,-3,INSTR(TIME_,' ')),SUBSTR(TIME_,1,INSTR(TIME_,' '))  
FROM SALES_DATA 
ORDER BY  SUBSTR(TIME_,-3,INSTR(TIME_,' ')),SUBSTR(TIME_,1,INSTR(TIME_,' '))DESC ;
--
SELECT TRIM(SUBSTR(TRACKING_NUMBER,-3,3)),DELIVERY_STATUS 
FROM SALES_DATA 
ORDER BY TRIM(SUBSTR(TRACKING_NUMBER,-3,3)),DELIVERY_STATUS ;
--
SELECT CUSTOMER_CITY,PRODUCT_BRAND,UNIT_PRICE
FROM SALES_DATA 
WHERE CUSTOMER_CITY LIKE('_�%')
ORDER BY CUSTOMER_CITY;
--
SELECT COUNT(*) AS DOLU,1000-COUNT(*)AS BOS
FROM SALES_DATA 
WHERE CUSTOMER_STATE IS NOT NULL AND CUSTOMER_ZIP IS NOT NULL
--
SELECT PRODUCT_BRAND as "Evvelki brand adi" ,REPLACE(PRODUCT_BRAND,'Nike','Adidas') as "Yeni brand adi",unit_price as "Indiki qiymet",unit_price*1.3 as "Artmis qiymet"
FROM SALES_DATA
WHERE unit_price>900
order by REPLACE(PRODUCT_BRAND,'Nike','Adidas'),to_number(unit_price)
