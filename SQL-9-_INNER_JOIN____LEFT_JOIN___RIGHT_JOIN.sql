Group 1 - INNER JOIN, LEFT JOIN, RIGHT JOIN:

Task 1:
Description: Retrieve the employee ID, first name, department name, job title, and country name of all employees who have a department assigned. 
Include employees who dont have a job title or a country assigned.
Tables: EMPLOYEES, DEPARTMENTS, JOBS, COUNTRIES
Hint: Use LEFT JOIN to combine the EMPLOYEES and DEPARTMENTS tables on the department ID column. 
Then use INNER JOIN to join the resulting table with the JOBS and COUNTRIES tables on the corresponding columns.

select e.employee_ID,e.first_name,e.last_name,d.department_name,j.job_title,l.country_id, c.country_name
from employees e 
left join departments d on e.department_id=d.department_id
left join jobs j on e.job_ID=j.job_id
left join locations l on d.location_id=l.location_id
left join countries c on l.country_id=c.country_id
where j.job_title is null or c.country_name is null;

Task 2:
Description: Retrieve the employee ID, first name, department name, job title, 
and country name of all employees along with their job history, if available. Include employees who dont have a department or job history.
Tables: EMPLOYEES, DEPARTMENTS, JOBS, COUNTRIES, JOB_HISTORY
Hint: Use LEFT JOIN to combine the EMPLOYEES, DEPARTMENTS, JOBS, and COUNTRIES tables on the corresponding columns. Then use LEFT JOIN to join the resulting table with the JOB_HISTORY table on the employee ID column.

select e.employee_ID,e.first_name,d.department_name,j.job_title,c.country_name,jh.start_date
from employees e 
left join departments d on e.department_id=d.department_id
left join jobs j on e.job_ID=j.job_id
left join locations l on d.location_id=l.location_id
left join countries c on l.country_id=c.country_id
left join job_history  jh on e.employee_id=jh.employee_id
where d.department_name is null or jh.start_date is null;

Task 3:
Description: Retrieve the employee ID, first name, department name, job title, 
and country name of all employees along with their corresponding department information. Include departments that dont have any employees assigned.
Tables: EMPLOYEES, DEPARTMENTS, JOBS, COUNTRIES
Hint: Use RIGHT JOIN to combine the EMPLOYEES, DEPARTMENTS, JOBS, and COUNTRIES tables on the corresponding columns.

select e.employee_ID,e.first_name,d.department_name,j.job_title,c.country_name
from employees e 
FULL join departments d on e.department_id=d.department_id
left join jobs j on e.job_ID=j.job_id
left join locations l on d.location_id=l.location_id
left join countries c on l.country_id=c.country_id
where e.employee_ID is null;


Task 4:
Description: Retrieve the employee ID, first name, department name, job title, 
and country name of all employees along with their corresponding job title and country information. Include employees who dont have a department or job title.
Tables: EMPLOYEES, DEPARTMENTS, JOBS, COUNTRIES
Hint: Use RIGHT JOIN to combine the EMPLOYEES, DEPARTMENTS, JOBS, and COUNTRIES tables on the corresponding columns.

select e.employee_ID,e.first_name,d.department_name,j.job_title,c.country_name
from departments d
right join  employees e  on e.department_id=d.department_id
left join jobs j on e.job_ID=j.job_id
left join locations l on d.location_id=l.location_id
left join countries c on l.country_id=c.country_id
where d.department_name is null or j.job_title is null;

Task 5:
Description: Retrieve the employee ID, first name, department name, job title, 
and country name of all employees along with their corresponding department and job title information. Include employees who dont have a country assigned.
Tables: EMPLOYEES, DEPARTMENTS, JOBS, COUNTRIES
Hint: Use INNER JOIN to combine the EMPLOYEES, DEPARTMENTS, and JOBS tables on the corresponding columns.
Then use RIGHT JOIN to join the resulting table with the COUNTRIES table on the country ID column.

select * from locations;
select * from employees;
select * from departments;
select * from jobs;
select * from countries;

select e.employee_ID,e.first_name,d.department_name,j.job_title,c.country_name
from employees e 
left join departments d on e.department_id=d.department_id
 join jobs j on e.job_ID=j.job_id
 left join locations l on d.location_id=l.location_id
left join countries c on l.country_id=c.country_id
where e.employee_ID is not null and c.country_name is null;
