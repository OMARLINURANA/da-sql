--Her departamentde calisan butun emekdaslarinin   departament uzre ortalama maasi yani avg -i 
--butun sirket uzre ortalama maasin yani avg- nece % teskil edirse,onu her dep uzre faizle gostermek lazimdir.
--Nerticede ekranda departament adi, department uzre avg salary ve sirket uzre avg salary ve % nisbeti--

select e.department_id,d.department_name,e.salary  "Emek haqqi",round(avg(e.salary)over(partition by e.department_id)) "Sobe uzre ortalama",
round(e.salary*100/round(avg(e.salary)over(partition by e.department_id))) ||' '||'%'  "Sobe uzre faiz nisbeti" ,
round(avg(e.salary)over())   "Sirket uzre ortalama",round(e.salary*100/round(avg(e.salary)over())) ||' '|| '%'   "Sirket uzre faiz nisbeti"
from employees e left join departments d
on e.department_id=d.department_id;
--
select d.department_name,a.salary  "Emek haqqi",avg_sal "Sobe uzre ortalama",round(a.salary*100/a.avg_sal)||' '|| '%'  "Sobe uzre faiz nisbeti",
avg_sal2  "Sirket uzre ortalama",round(a.salary*100/a.avg_sal2)||' '|| '%'   "Sirket uzre faiz nisbeti"
from(select e.*,round(avg(salary) over(partition by department_id)) as avg_sal,round(avg(salary) over() )as avg_sal2 from employees e) a 
left join departments d
on a.department_id=d.department_id;

--Sirketde calisan emektaslarin butun melumatlarini ve elecede departament adini ekranda gostermek

select e.*,d.department_name 
from employees e left join departments d 
on e.department_id=d.department_id;

select count(*)
from employees e left join departments d 
on e.department_id=d.department_id;

--Sirketde calisan adlari a herfi ile baslamayan ve salarysi 5000 den cox olan emektaslarin melumatlari--
--Sirketde calisan adlari a herfi ile baslayan ve salarysi 5000 den az olan emektaslarin melumatlari birge ekranda gostermek--

select * from employees 
where salary>5000 and first_name not like 'A%'
union all
select * from employees 
where salary<5000 and first_name  like 'A%';

--Hansi illerde emektaslar ise goturulub o illeri ayrica sutunda gostermek lazimdir.

select extract(year from to_date(hire_date,'dd/mm/yy'))from employees;
--
select extract(month from to_date(hire_date,'dd/mm/yy'))from employees;
--
select to_char ( hire_date, ' yyyy' )  from employees;
--
select to_char ( hire_date, ' Month' )  from employees;
--
select extract (year from hire_date)from employees;
--
select extract (month from hire_date)from employees;

--hansi heftenin gunu emeklataslar ise goturulub o heftenin gunlerini ayrica sutunda gostermek

select e.*,to_char ( e.hire_date,'Day') as "Hire weekday" from employees e;
--telefonun son 4 reqemlerini ekranda gostermek.
--
select substr(phone_number,9,4) from employees;
--
select substr(phone_number,instr(phone_number,'.',1,2)+1,4) from employees;
--
select substr(phone_number,instr(phone_number,'.',1,2)+1,length(phone_number)-instr(phone_number,'.',1,1)) from employees;
--
select substr(phone_number,-4,4) from employees;   
--
select substr(phone_number,length(phone_number)-instr(phone_number,'.',1)+1) from employees;


