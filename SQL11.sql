Task 6:
Find all departments, whether they have employees or not.

Hint: Use LEFT JOIN on "departments" and "employees".

select count(e.employee_id),d.department_name 
from departments d left join employees e
on e.department_id=d.department_id
group by d.department_name

Task 7: 
Find all employees, even if their department doesnt exist.
Hint: Use RIGHT JOIN on "departments" and "employees".

select e.employee_id ,d.department_name
from departments d right join employees e
on e.department_id=d.department_id;
--
select e.employee_id ,d.department_name
from departments d right join employees e
on e.department_id=d.department_id
where department_name is null;

Task 8:
Show all employees and their managers, even if they dont have a manager.
Hint: Use RIGHT JOIN on the "employees" table twice.

select e.employee_id,e.first_name,f.manager_id
from employees e left join employees f
on e.manager_id=f.employee_id;

select distinct manager_id from employees;
select e.employee_id,e.first_name,f.manager_id
from employees f right join employees e
on e.employee_id=f.employee_id 
 right join employees f on e.employee_id=f.employee_id ;
 
Task 9:
Display all employees and departments, whether they are assigned or not.

Hint: Use FULL OUTER JOIN on "employees" and "departments".

select e.employee_id,e.first_name,d.department_id,d.department_name
from employees e  full join departments d
on e.department_id=d.department_id;

Task 10:
Show all locations and departments, whether they have a department or not.

Hint: Use FULL OUTER JOIN on "locations" and "departments".

select l.location_id,l.street_address,d.department_id,d.department_name
from  departments d full join locations l
on d.location_id=l.location_id;
