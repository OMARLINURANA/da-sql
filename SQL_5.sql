SELECT * FROM SALES_DATA;
--
SELECT  YEAR_,QUARTER,SUM(TOTAL_PRICE)
FROM SALES_DATA
WHERE YEAR_ IN(2022,2023)
GROUP BY YEAR_,QUARTER
ORDER BY QUARTER,YEAR_;
!
SELECT  YEAR_,QUARTER,SUM(TOTAL_PRICE)
FROM SALES_DATA
WHERE YEAR_ IN(2022,2023)AND QUARTER IN('Q2','Q3')
GROUP BY YEAR_,QUARTER
ORDER BY QUARTER,YEAR_;
!
SELECT  YEAR_,QUARTER,SUM(TOTAL_PRICE)
FROM SALES_DATA
WHERE YEAR_=2023 AND QUARTER IN('Q2','Q3')
GROUP BY YEAR_,QUARTER
ORDER BY QUARTER,YEAR_;
!
--1.Yaranmis qiymet ferqine gore satisin dusmesi sebebile satis qiymetlerinde 15% endirim edilmesi
SELECT YEAR_,QUARTER,product_id,UNIT_PRICE as "Evvelki qiymmet",UNIT_PRICE*0.85 as "Endirimli qiymet"
FROM SALES_DATA 
WHERE YEAR_=2023 AND QUARTER IN('Q2','Q3');
--
SELECT YEAR_,QUARTER,PRODUCT_SUBCATEGORY,QUANTITY,TOTAL_PRICE
FROM SALES_DATA 
WHERE YEAR_=2023 AND QUARTER IN('Q2','Q3')
ORDER BY QUARTER,TO_NUMBER(TOTAL_PRICE)DESC;
!
SELECT QUARTER,MIN(TOTAL_PRICE)
FROM SALES_DATA 
WHERE YEAR_=2023 AND QUARTER IN('Q2','Q3')
GROUP BY QUARTER;
-- 2.Eyni rubluk erzinde en az satis(azn) etmis subcategory mehsullarina 30 % endirim qiymeti ile satilmasi
SELECT PRODUCT_SUBCATEGORY,QUANTITY,TOTAL_PRICE,product_id,UNIT_PRICE as "Evvelki qiymmet",UNIT_PRICE*0.7 as "Endirimli qiymet"
FROM SALES_DATA 
WHERE YEAR_=2023  AND PRODUCT_SUBCATEGORY in('Exercise Equipment','Board Games')
--
SELECT *
FROM SALES_DATA 
WHERE YEAR_=2023 AND QUARTER IN('Q2','Q3');
--3.Eyni rublukte qaytarilma sebebi  qusurlu mehsuldur olan mehsullardan qiymeti 600 den cox olanlarda 35%,600den az olanlarda ise 50 % endirimle musteriye teqdim edilmesi.
SELECT return_reason,unit_price as "Evvelki qiymmet",unit_price*0.5 as "Endirimli qiymet"
FROM SALES_DATA 
WHERE YEAR_=2023 AND QUARTER IN('Q2','Q3') and return_reason='Defective Item' and unit_price<600
union all
SELECT return_reason,unit_price,unit_price*0.65
FROM SALES_DATA 
WHERE YEAR_=2023 AND QUARTER IN('Q2','Q3') and return_reason='Defective Item' and unit_price>600;
--4.Eyni rublukte vergisi cox olan , satisi ve umumi geliri az olan mehsullarin deaktiv edilmesi,30% endirim tetbiq edilmesi
SELECT product_code,product_id,quantity,total_price,tax,unit_price as "Evvelki qiymmet",unit_price*07 as "Endirimli qiymet"
FROM SALES_DATA 
WHERE YEAR_=2023 AND QUARTER IN('Q2','Q3')and tax>=30 and quantity<600 and total_price<4000
order by   to_number(tax) desc,to_number(quantity);
--5.Eyni rubluk erzinde vergisi ortalama (30-70) satis deyeri musteri kutlesinin budcesine uygun ortalama bir deyer ve asagisi olan mehsullar uzerinde kompaniyalar ,reklam tetbiqi ile musteri celb edilmesi,bu mehsullar uzerinden satisin artirilmasi.
SELECT product_code,product_id,unit_price,quantity,total_price,tax
FROM SALES_DATA 
WHERE YEAR_=2023 AND QUARTER IN('Q2','Q3')and tax>30 and tax<70 and unit_price<500 and total_price>5000
order by   to_number(tax) desc,to_number(quantity)

